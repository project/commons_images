<?php
/**
 * @file
 * commons_images.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function commons_images_field_default_field_instances() {
  $field_instances = array();

  // Get a list of content types that should have the Image Insert field added.
  $commons_images_entity_types = commons_images_get_images_entity_types();
  if (!empty($commons_images_entity_types)) {
    foreach ($commons_images_entity_types as $entity_type => $bundles) {
      foreach ($bundles as $bundle => $settings) {
        commons_images_field_definition($field_instances, $entity_type, $bundle, $settings);
      }
    }
  }

  // Translatables
  // Included for use with string extractors like potx.
  t('Add images');
  t('After uploading an image, click "Insert" to add the image to the content body.');

  return $field_instances;
}

/**
* Contains a field definition export for the Image Insert field for re-use
* across content types.
*/
function commons_images_field_definition(&$field_instances, $entity_type, $bundle, $settings) {
  // Set default image_insert settings and override if disabled.
  $description = 'After uploading an image, click "Insert" to add the image to the content body.';
  $image_insert_settings = array(
    'insert' => 1,
    'insert_absolute' => 0,
    'insert_class' => '',
    'insert_default' => 'auto',
    'insert_styles' => array(
      'auto' => 0,
      'icon_link' => 0,
      'image' => 'image',
      'image_35x35' => 0,
      'image_50x50' => 0,
      'image_50x50_avatar' => 0,
      'image_large' => 'image_large',
      'image_medium' => 'image_medium',
      'image_rich_snippets_thumbnail' => 0,
      'image_thumbnail' => 0,
      'link' => 0,
    ),
    'insert_width' => '',
  );
  if (isset($settings['exclude_image_insert']) && $settings['exclude_image_insert'] == TRUE) {
    $description = '';
    $image_insert_settings['insert'] = 0;
    $image_insert_settings['insert_styles'] = array();
  }

  // Set default filefield_sources settings and override if enabled.
  $filefield_sources_settings = array(
    'filefield_sources' => array(
      'filefield_sources' => array(
        'upload' => 'upload',
      ),
    ),
  );
  if (isset($settings['enable_plupload']) && $settings['enable_plupload'] == TRUE) {
    $description = 'Select multiple files or drag & drop them in the box. Then click Start Upload';
    $filefield_sources_settings['filefield_sources']['filefield_sources']['upload'] = '';
    $filefield_sources_settings['filefield_sources']['filefield_sources']['plupload'] = 'plupload';
  }

  $field_instances["$entity_type-$bundle-field_images"] = array(
    'bundle' => $bundle,
    'deleted' => '0',
    'description' => $description,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'colorbox',
        'settings' =>  array(
          'colorbox_caption' => 'auto',
          'colorbox_gallery' => 'post',
          'colorbox_image_style' => 'large',
          'colorbox_node_style' => 'thumbnail',
        ),
        'type' => 'colorbox',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => $entity_type,
    'field_name' => 'field_images',
    'label' => 'Add images',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => "$bundle/images",
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '16 MB',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      )
      + $image_insert_settings
      + $filefield_sources_settings,
      'type' => 'image_image',
      'weight' => '2',
    ),
  );
}
